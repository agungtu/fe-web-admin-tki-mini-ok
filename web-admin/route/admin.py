# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

from fastapi import APIRouter, Request, Depends,Form
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse,RedirectResponse
import requests
import starlette.status as status

# from fastapi import APIRouter, Request, Depends,Form
# from fastapi.security import OAuth2PasswordRequestForm
# from fastapi.templating import Jinja2Templates
# from fastapi.responses import HTMLResponse,RedirectResponse
# import requests
# import starlette.status as status

router_admin = APIRouter()

templates = Jinja2Templates(directory="./admin")

@router_admin.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})
    

@router_admin.get("/signup", response_class=HTMLResponse)
async def get_signup(request: Request):
    return templates.TemplateResponse("signup.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/home", response_class=HTMLResponse)
async def get_home(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/user", response_class=HTMLResponse)
async def get_leads(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"user"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/logout", response_class=HTMLResponse)
async def get_logout(request: Request):
    request.session['token'] = None
    return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

  # TESTIMONY SHOW DATA
@router_admin.get("/testimony", response_class=HTMLResponse)
async def get_testimony(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/get_testimony/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"testimony", "data": response["content"]})

# ADD TESTIMONY

@router_admin.post("/testimony", response_class=HTMLResponse)
async def add_testimony(
        request: Request, 
        nama: str = Form(...),
        profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/testimony"
    dataa = {"nama":nama,"profesi":profesi, "title": title, "description": description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(token)
    # return res.json()
    # return str(response)
    return RedirectResponse("/web_admin/testimony", status_code=status.HTTP_302_FOUND)

#edit testimonny
@router_admin.post("/testimonyy", response_class=HTMLResponse)
async def edit_testimony(
        request: Request, 
        nama: str = Form(...),
        profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/testimony/" + _id
    dataa = {"nama":nama,"profesi":profesi, "title": title, "description": description, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/testimony", status_code=status.HTTP_302_FOUND)

#Delete testimony 
@router_admin.get("/testimony/delete/{id_}")
async def delete_testimony(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/testimony/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/testimony", status_code=status.HTTP_302_FOUND) 


#show data about
@router_admin.get("/about", response_class=HTMLResponse)
async def get_about(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_about/get_about/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"about", "data": response["content"]})

#add data about
@router_admin.post("/about", response_class=HTMLResponse)
async def add_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_about/about"
    dataa = {"title":title,"description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/about", status_code=status.HTTP_302_FOUND)

#edit about
@router_admin.post("/aboutt", response_class=HTMLResponse)
async def edit_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_about/about/" + _id
    dataa = {"title": title, "description": description, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/about", status_code=status.HTTP_302_FOUND) 

#delete about
@router_admin.get("/about/delete/{id_}")
async def delete_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_about/about/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/about", status_code=status.HTTP_302_FOUND) 

#show data News
@router_admin.get("/news", response_class=HTMLResponse)
async def get_news(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_news/get_news/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"news", "data": response["content"]})

# ADD news
@router_admin.post("/news", response_class=HTMLResponse)
async def add_news(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_news/news"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/news", status_code=status.HTTP_302_FOUND)


 #edit News
@router_admin.post("/newsyuk", response_class=HTMLResponse)
async def edit_news(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_news/news/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/news", status_code=status.HTTP_302_FOUND)

@router_admin.get("/news/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_news/news/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/news", status_code=status.HTTP_302_FOUND)

#show data faq
@router_admin.get("/faq", response_class=HTMLResponse)
async def get_faq(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_faq/get_faq"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"faq", "data": response["content"]})

# ADD FAQ
@router_admin.post("/faq", response_class=HTMLResponse)
async def add_faq(
        request: Request, 
        question: str = Form(...),
        answer: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_faq/faq"
    dataa = {"question":question, "answer":answer, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/faq", status_code=status.HTTP_302_FOUND)

    #edit faq
@router_admin.post("/faqq", response_class=HTMLResponse)
async def edit_faqq(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        question: str = Form(...),
        answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_faq/faq/" + _id
    dataa = {"question": question, "answer": answer, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/faq", status_code=status.HTTP_302_FOUND) 

#delete faq
@router_admin.get("/faq/delete/{id_}")
async def delete_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_faq/faq/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/faq", status_code=status.HTTP_302_FOUND)

#SHOW ACHIEVEMENT
@router_admin.get("/achievement", response_class=HTMLResponse)
async def get_achievement(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/get_achievement/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"achievement", "data": response["content"]})

# ADD ACHIEVEMENT
@router_admin.post("/achievement", response_class=HTMLResponse)
async def add_achievement(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/achievement"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/achievement", status_code=status.HTTP_302_FOUND)

    #edit ACHIEVEMENT
@router_admin.post("/achievementnih", response_class=HTMLResponse)
async def edit_achievementnih(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/achievement/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/achievement", status_code=status.HTTP_302_FOUND) 

    #delete ACHIEVEMENNT
@router_admin.get("/achievementnihh/delete/{id_}")
async def delete_achievementnihh(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/achievement/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/achievement", status_code=status.HTTP_302_FOUND)

#SHOW FEATURE
@router_admin.get("/feature", response_class=HTMLResponse)
async def get_feature(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_feature/get_feature/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"feature", "data": response["content"]})

# ADD FEATURE
@router_admin.post("/feature", response_class=HTMLResponse)
async def add_feature(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_feature/feature"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/feature", status_code=status.HTTP_302_FOUND)

 #edit FEATURE
@router_admin.post("/featuree", response_class=HTMLResponse)
async def edit_featuree(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_feature/feature/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/feature", status_code=status.HTTP_302_FOUND) 
  
  
#delete FEATURE
@router_admin.get("/feature/delete/{id_}")
async def delete_feature(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"feature", "tipe":"feature"})

    url = "http://127.0.0.1:8001/web_api/web_feature/feature/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/feature", status_code=status.HTTP_302_FOUND)

#SHOW PRICING
@router_admin.get("/pricing", response_class=HTMLResponse)
async def get_pricing(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/get_pricing/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"pricing", "data": response["content"]})

  
# ADD pricing
@router_admin.post("/pricing", response_class=HTMLResponse)
async def add_pricing(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/pricing"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/pricing", status_code=status.HTTP_302_FOUND)

#edit FEATURE
@router_admin.post("/pricingg", response_class=HTMLResponse)
async def edit_pricingg(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/pricing/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/pricing", status_code=status.HTTP_302_FOUND) 
 
#delete PRICING
@router_admin.get("/pricing/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/pricing/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/pricing", status_code=status.HTTP_302_FOUND)

#SHOW PARTNER
@router_admin.get("/partner", response_class=HTMLResponse)
async def get_partner(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_partner/get_partner/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"partner", "data": response["content"]})

# ADD PARTNER
@router_admin.post("/partner", response_class=HTMLResponse)
async def add_partner(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_partner/partner"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/partner", status_code=status.HTTP_302_FOUND)

#edit PARTNER
@router_admin.post("/partnerr", response_class=HTMLResponse)
async def edit_partnerr(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_partner/partner/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/partner", status_code=status.HTTP_302_FOUND) 

#delete PARTNER
@router_admin.get("/partnerrr/delete/{id_}")
async def delete_partnerrr(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_partner/partner/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/partner", status_code=status.HTTP_302_FOUND)

#SHOW GALLERY
@router_admin.get("/gallery", response_class=HTMLResponse)
async def get_gallery(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/get_gallery/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"gallery", "data": response["content"]})

# ADD GALLERY
@router_admin.post("/gallery", response_class=HTMLResponse)
async def add_gallery(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/gallery"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/gallery", status_code=status.HTTP_302_FOUND)

    #edit GALLERY
@router_admin.post("/galleryy", response_class=HTMLResponse)
async def edit_galleryy(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/gallery/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/gallery", status_code=status.HTTP_302_FOUND) 

#delete GALLERY
@router_admin.get("/galleryui/delete/{id_}")
async def delete_galleryui(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/gallery/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/gallery", status_code=status.HTTP_302_FOUND)

#SHOW VP
@router_admin.get("/videoproduct", response_class=HTMLResponse)
async def get_videoproduct(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/get_video_product/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"videoproduct", "data": response["content"]})

# ADD VP
@router_admin.post("/videoproduct", response_class=HTMLResponse)
async def add_videoproduct(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/video_product"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/videoproduct", status_code=status.HTTP_302_FOUND)

 #edit VP
@router_admin.post("/videoproductt", response_class=HTMLResponse)
async def edit_videoproductt(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/video_product/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/videoproduct", status_code=status.HTTP_302_FOUND)

#delete VP
@router_admin.get("/videoproducttt/delete/{id_}")
async def delete_videoproducttt(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/video_product/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/videoproduct", status_code=status.HTTP_302_FOUND)

#SHOW wekkly
@router_admin.get("/weekly", response_class=HTMLResponse)
async def get_weekly(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/get_weekly/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"weekly", "data": response["content"]})

# ADD weekly
@router_admin.post("/weekly", response_class=HTMLResponse)
async def add_weekly(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/weekly"
    dataa = {"title":title, "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/weekly", status_code=status.HTTP_302_FOUND)

#edit WEEKLY
@router_admin.post("/weeklyy", response_class=HTMLResponse)
async def edit_weeklyy(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/weekly/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/weekly", status_code=status.HTTP_302_FOUND)

#delete weekly
@router_admin.get("/weeklyyy/delete/{id_}")
async def delete_weeklyyy(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/weekly/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/weekly", status_code=status.HTTP_302_FOUND)

#SHOW SLIDER
@router_admin.get("/slider", response_class=HTMLResponse)
async def get_slider(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://127.0.0.1:8001/web_api/web_slider/get_slider/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"slider", "data": response["content"]})

# ADD SLIDER
@router_admin.post("/slidera", response_class=HTMLResponse)
async def add_slider(
        request: Request, 
        title: str = Form(...),
        # tipe: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_slider/slider"
    dataa = {"title":title,  "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/slider", status_code=status.HTTP_302_FOUND)

#edit SLIDER
@router_admin.post("/sliderr", response_class=HTMLResponse)
async def edit_sliderr(
        request: Request, 
        title: str = Form(...),
        # tipe: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_slider/slider/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/slider", status_code=status.HTTP_302_FOUND)

#delete slider
@router_admin.get("/sliderrr/delete/{id_}")
async def delete_sliderrr(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_slider/slider/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/slider", status_code=status.HTTP_302_FOUND)

    #SHOW PRODUCT
@router_admin.get("/product", response_class=HTMLResponse)
async def get_product(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/get_product/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"product", "data": response["content"]})

# ADD PRODUCT
@router_admin.post("/product", response_class=HTMLResponse)
async def add_product(
        request: Request, 
        title: str = Form(...),
        tipe: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/product/"
    dataa = {"title":title, "tipe": tipe, "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/product", status_code=status.HTTP_302_FOUND)

#edit PRODUCT
@router_admin.post("/productt", response_class=HTMLResponse)
async def edit_productt(
        request: Request, 
        title: str = Form(...),
        tipe: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/product/" + _id
    dataa = {"title": title, "tipe": tipe, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/product", status_code=status.HTTP_302_FOUND)

#delete product
@router_admin.get("/producttt/delete/{id_}")
async def delete_producttt(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/product/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/product", status_code=status.HTTP_302_FOUND)
