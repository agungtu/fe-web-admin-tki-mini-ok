# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

from fastapi import FastAPI, Request, Depends
from fastapi.security import OAuth2PasswordRequestForm
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from starlette.middleware.sessions import SessionMiddleware
import requests
from starlette.responses import RedirectResponse

app = FastAPI(openapi_url="/coba/openapi.json",docs_url="/coba/swgr")
app.mount("/coba/static", StaticFiles(directory="static"), name="static")


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

app.add_middleware(SessionMiddleware, secret_key='ifpeoiu83iueoi9028')

@app.get("/health")
async def health():
    return {"status": "ok"}

templates = Jinja2Templates(directory="coba")

@app.get("/coba/home", response_class=HTMLResponse)
async def get_home(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})
